<?php 
  $host = 'localhost:3306';  
  $user = 'root';  
  $pass = ''; 
  $dbName = 'pixel6_employee'; 
  $conn =  mysqli_connect($host,$user,$pass,$dbName);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>View All Employee</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
        <a href="index.php">Back</a>
        <table class="table table-striped">
            <tr>
                <td>Id</td>
                <td>Name</td>
                <td>Mobile</td>
                <td>Email</td>
                <td>Graduation</td>
                <td>Graduation %</td>
                <td>Exprince</td>
            </tr>            
                 <?php 
                    if(!$conn){
                        die("connection not stablished..".mysqli_connect_error());
                    }
                    else{
                        $sql ="SELECT * FROM employee,emp_education,emp_exp WHERE employee.emp_id=emp_education.emp_id AND employee.emp_id=emp_exp.emp_id";
                        $data = mysqli_query($conn,$sql);
                        if ($data->num_rows > 0) {
                            while($row = $data->fetch_assoc()) {
                                echo "<tr>";
                                echo  "<td>".$row["emp_id"]."</td>";
                                echo  "<td>".$row["emp_name"]."</td>";
                                echo  "<td>".$row["emp_mobile"]."</td>";
                                echo  "<td>".$row["emp_email"]."</td>"; 
                                echo  "<td>".$row["graduation"]."</td>";
                                echo  "<td>".$row["grad_per"]."</td>";  
                                echo  "<td>".$row["exp_year"]."</td>"; 
                            }
                        } 
                        else {
                            echo "0 results";
                        }
                       
                    }
                    mysqli_close($conn);                   
                  ?>                 
            </tr>
        </table>
    </body>
