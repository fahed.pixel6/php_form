<!DOCTYPE html>
<html>
    <head>
        <title>Resume</title>
        <link rel="stylesheet" href="./css/reset.css">
        <link rel="stylesheet" href="./css/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <!-- <script src="./js/script.js"></script> -->
    </head>
    <body>              
        <section class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 bg-light">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Application</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="view.php">Application View</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <form action="getinfo.php"  method="POST" onsubmit="validate(event);">
                    <div class="row">
                        <div class="col-md-4 form-group">                    
                           <label for="title">Title:</label>
                           <select class="form-control" id="title" name="title">
                               <option value="0">SELECT</option>
                               <option value="Mr">Mr.</option>
                               <option value="Mrs">Mrs.</option>
                           </select>       
                           <p id="title-error" class="text-danger"></p>                     
                        </div>
                        <div class="col-md-8 form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"/>
                            <p id="name-error" class="text-danger"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                           <label for="father-name">Father Name</label>
                           <input type="text" class="form-control" id="father-name" placeholder="Enter Father Name" name="fname">
                           <p id="fname-error" class="text-danger"></p>
                        </div>
                        <div class="col-md-6 form-group">
                          <label for="dob">Date Of Birth</label> 
                          <input type="date" class="form-control" id="dob" name="dob">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="mob-no">Mobile Number</label>
                             <input type="text" class="form-control" id="mob-no" placeholder="Enter Mobile Number" name="mobile" maxlength="10" >
                             <p id="mob-error" class="text-danger"></p>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="email-id">Email</label>
                            <input type="email" class="form-control" id="email-id" placeholder="Enter email" name="email">
                            <p id="email-error" class="text-danger"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="address">Address</label>
                            <textarea class="form-control" id="address" name="address"></textarea>
                            <p id="address-error" class="text-danger"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <label for="male-gender">Gender</label>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="gender" value="male" id="male-gender">Male
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label" >
                                    <input type="radio" class="form-check-input" name="gender" value="female" id="female-gender">Female
                                </label>
                            </div>
                            <div class="form-check disabled">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="gender" value="other">Other
                                </label>
                            </div>
                            <p id="gender-error" class="text-danger"></p>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-md-12 form-group">
                            <label for="objective">Objective</label>
                            <input type="text" class="form-control" placeholder="Enter Objective" id="objective" name="objectiveinput">
                            <p id="objectiveinput-error" class="text-danger"></p>
                        </div>                        
                    </div>
                    <div class="row col-md-12">
                        <label for="exp-year">Exprince</label>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>Year</label>
                            <select class="form-control" id="exp-year" name="exp-year">
                                <?php 
                                  $expYear=array("select year",0,1,2,3,4,5,6,7,8,9,10);
                                  $val=0;
                                  foreach ($expYear as $value) {
                                    echo "<option value='$val'>$value</option>";
                                    $val++;
                                  }
                                ?>
                            </select> 
                            <p id="exp-error" class="text-danger"></p>                          
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Month</label>
                            <select class="form-control" id="exp-month" name="exp-month">
                                <?php
                                $expMonth=array("select Month",1,2,3,4,5,6,7,8,9,);
                                $val=0;
                                foreach ($expMonth as $value) {
                                    echo "<option value=$val> $value</option>";
                                    $val++;
                                }
                                ?>
                            </select>
                            <p id="expmonth-error" class="text-danger"></p>
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="details">Details</label>
                            <input type="text" class="form-control" id="details" placeholder="Enter Details" name="skills"> 
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <label>Education Details</label>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>10th</label>
                            <input type="text" class="form-control" placeholder="%" id="ssc-per" name="ssc-per">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Board</label>
                            <select class="form-control" name="board">
                                <option>Amravati Board</option>
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Select Passing Year</label>
                            <select class="form-control" name="ssc-year">
                                <?php 
                                $year=2010;
                                for($i=0;$i<=9;$i++){
                                    $year++;
                                   echo "<option value=$year>$year</option>";
                                }
                                ?>
                            </select> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>12th</label>
                            <input type="text" class="form-control" placeholder="%" id="hsc-per" name="hsc-per">
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Board</label>
                            <select class="form-control" name="hsc-board">
                                <option value="Amravati Board">Amravati Board</option>
                            </select>
                        </div>
                        <div class="col-md-4 form-group">
                            <label>Select Passing Year</label>
                            <select class="form-control" name="hsc-passing">
                                <option disabled selected>Select Year</option>
                                <option value="2006">2006</option>
                                <option value="2007">2007</option>
                                <option value="2008">2008</option>
                                <option value="2009">2009</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label>Graduation</label>
                            <select class="form-control" name="graduation">
                                <option disabled selected>Select Graduation</option>
                                <option>B.E</option>
                                <option>B.Tech</option>
                                <option>BSC</option>
                                <option>BCA</option>
                            </select>
                        </div>
                        <div class="col-md-3 form-group">
                            <label>Mark %</label>
                            <input type="text" class="form-control" name="grad-per">
                        </div>
                        <div class="col-md-3 form-group">
                             <label>College/University</label>
                            <input type="text" class="form-control" name="grad-clg">
                        </div>
                       <div class="col-md-3 form-group">
                           <label>year of passing</label>
                           <select class="form-control" name="grad-year">
                                <option disabled selected>Select Year</option>
                                <option>2008</option>
                                <option>2009</option>
                                <option>2010</option>
                                <option>2011</option>
                            </select>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 form-group">
                            <label>Post Graduation</label>
                            <select class="form-control" name="post-grad">
                                <option disabled selected>Select Post Graduation</option>
                                <option>M.E</option>
                                <option>M.Tech</option>
                                <option>MSC</option>
                                <option>MCA</option>
                            </select>
                        </div>
                        <div class="col-md-3 form-group">
                            <label>Mark %</label>
                            <input type="text" class="form-control" name="post-per">
                        </div>
                        <div class="col-md-3 form-group">
                            <label>College/University</label>
                            <input type="text" class="form-control" name="post-clg">
                        </div>
                        <div class="col-md-3 form-group">
                            <label>year of passing</label>
                            <select class="form-control" name="post-year">
                                <option disabled selected>Select Year</option>
                                <option>2008</option>
                                <option>2009</option>
                                <option>2010</option>
                                <option>2011</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                           <label>Skills</label>
                           <input type="text" class="form-control" placeholder="Add skills eg. html,css,js etc." name="skills">                 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                           <label>Project Details</label>
                           <textarea type="text" class="form-control" name="project-details" >
                           </textarea>                 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Hobbies</label>
                            <input type="text" class="form-control" placeholder="eg.reading book,painting etc..">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label>Upload Image</label>
                            <input type="file" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success btn-lg" value="Submit">
                            <input type="reset" class="btn btn-danger btn-lg" value="Reset">
                        </div>
                    </div>
                </form>                
            </div>          
        </section>
    </body>
</html>
